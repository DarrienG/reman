all: x86 musl default

default:
	cargo build --release
	strip target/release/reman

x86:
	cross build --release --target aarch64-unknown-linux-gnu
	strip target/aarch64-unknown-linux-gnu/release/reman

musl:
	cross build --release --target x86_64-unknown-linux-musl
	strip target/x86_64-unknown-linux-musl/release/reman
