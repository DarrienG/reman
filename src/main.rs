use std::collections::BTreeSet;
use std::io;
use std::io::stdin;
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;
use tui::backend::TermionBackend;
use tui::layout::{Alignment, Constraint, Direction, Layout};
use tui::style::{Color, Style};
use tui::widgets::{Block, Borders, Paragraph, Text};
use tui::Terminal;

use word_determiner::ReMan;

mod word_determiner;

fn main() -> Result<(), io::Error> {
    let mut re_man = ReMan::new();

    let letters_guessed: &mut BTreeSet<char> = &mut BTreeSet::new();

    let strikes_displayed = &mut vec![];
    for _ in 0..re_man.max_strikes() {
        strikes_displayed.push('o');
    }

    loop {
        let stdout = io::stdout().into_raw_mode()?;
        let stdout = AlternateScreen::from(stdout);
        let backend = TermionBackend::new(stdout);
        let mut terminal = Terminal::new(backend)?;
        terminal.hide_cursor()?;

        terminal.draw(|mut f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(1)
                .constraints(
                    [
                        Constraint::Percentage(20),
                        Constraint::Percentage(20),
                        Constraint::Percentage(55),
                        Constraint::Percentage(5),
                    ]
                    .as_ref(),
                )
                .split(f.size());

            f.render_widget(
                Paragraph::new(
                    &mut [Text::raw(
                        (*letters_guessed)
                            .clone()
                            .into_iter()
                            .map(|c| c.to_string())
                            .collect::<Vec<String>>()
                            .join(", "),
                    )]
                    .iter(),
                )
                .block(
                    Block::default()
                        .title("Guessed Letters")
                        .borders(Borders::ALL)
                        .border_style(get_border_styles(&re_man)),
                ),
                chunks[0],
            );

            f.render_widget(
                Paragraph::new(
                    &mut [Text::raw(
                        (*strikes_displayed)
                            .clone()
                            .iter()
                            .map(|c| c.to_string())
                            .collect::<Vec<String>>()
                            .join(" "),
                    )]
                    .iter(),
                )
                .block(
                    Block::default()
                        .title("Number Strikes Left")
                        .borders(Borders::ALL)
                        .border_style(get_border_styles(&re_man)),
                ),
                chunks[1],
            );

            f.render_widget(
                Paragraph::new(
                    &mut [Text::raw(format!(
                        "{}\n{}",
                        "_ _ _ _ _ _",
                        get_extra_loss_text(&re_man)
                    ))]
                    .iter(),
                )
                .block(
                    Block::default()
                        .title("ReMan")
                        .borders(Borders::ALL)
                        .border_style(get_border_styles(&re_man)),
                ),
                chunks[2],
            );

            f.render_widget(
                Paragraph::new(
                    &mut [Text::raw(format!(
                        "{}{}",
                        get_extra_help_text(&re_man),
                        "^C to quit"
                    ))]
                    .iter(),
                )
                .alignment(Alignment::Center),
                chunks[3],
            );
        })?;

        let stdin = stdin();
        let c = stdin.keys().find_map(Result::ok);
        match c.unwrap() {
            Key::Ctrl('c') => return Ok(()),
            Key::Char(c) => {
                if re_man.can_continue() {
                    for lower in c.to_lowercase() {
                        if letters_guessed.contains(&lower) {
                            continue;
                        }

                        let result = re_man.guess_letter(lower);

                        if !result {
                            strikes_displayed[re_man.get_num_strikes() as usize - 1] = 'x';
                        }

                        letters_guessed.insert(lower);
                    }
                }
            }
            _ => {}
        }
    }
}

fn get_extra_loss_text(re_man: &ReMan) -> String {
    if !re_man.can_continue() {
        let last_word = re_man
            .get_possible_words()
            .into_iter()
            .collect::<Vec<&str>>()[0];

        format!("You lost! The word was: {}", last_word)
    } else {
        "".to_string()
    }
}

fn get_extra_help_text(re_man: &ReMan) -> String {
    if re_man.can_continue() {
        "Type any letter to guess - ".to_string()
    } else {
        "".to_string()
    }
}

fn get_border_styles(re_man: &ReMan) -> Style {
    if re_man.can_continue() {
        Style::default()
    } else {
        Style::default().fg(Color::Red)
    }
}
