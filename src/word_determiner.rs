use std::collections::HashSet;

pub struct ReMan<'a> {
    possible_words: HashSet<&'a str>,
    num_strikes: u32,
    can_continue: bool,
    max_strikes: u8,
}

impl<'a> ReMan<'a> {
    pub fn new() -> ReMan<'a> {
        ReMan {
            possible_words: ["banana", "excess", "giggit", "voodoo", "pullup", "myrrhy"]
                .iter()
                .cloned()
                .collect(),
            num_strikes: 0,
            can_continue: true,
            max_strikes: 5,
        }
    }

    /// The user guesses a letter, but always gets a strike
    /// In this game, the user will always lose, so if a character is contained
    /// in a word, then we remove the word and add a strike,
    /// otherwise we just give a strike.
    pub fn guess_letter(&mut self, guess: char) -> bool {
        for word in self.possible_words.clone() {
            if word.contains(guess) {
                self.possible_words.remove(word);
                self.increment_max_strikes();
                return false;
            }
        }

        self.increment_max_strikes();
        false
    }

    pub fn get_possible_words(&self) -> HashSet<&'a str> {
        self.possible_words.clone()
    }

    fn increment_max_strikes(&mut self) {
        self.num_strikes += 1;
        self.can_continue = self.num_strikes < self.max_strikes.into();
    }

    pub fn get_num_strikes(&self) -> u32 {
        self.num_strikes
    }

    pub fn can_continue(&self) -> bool {
        self.can_continue
    }

    pub fn max_strikes(&self) -> u8 {
        self.max_strikes
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn does_remove_guessed_words() {
        let mut re_man = ReMan::new();

        re_man.guess_letter('a');
        assert!(!re_man.possible_words.contains("banana"));

        re_man.guess_letter('e');
        assert!(!re_man.possible_words.contains("excess"));
    }

    #[test]
    fn does_increment_strike() {
        let mut re_man = ReMan::new();

        re_man.guess_letter('a');
        re_man.guess_letter('e');
        assert_eq!(2, re_man.get_num_strikes());
    }

    #[test]
    fn cannot_continue() {
        let mut re_man = ReMan::new();

        re_man.guess_letter('a');
        re_man.guess_letter('j');

        assert!(re_man.can_continue());

        re_man.guess_letter('p');
        re_man.guess_letter('k');
        re_man.guess_letter('l');

        assert!(!re_man.can_continue());
    }
}
