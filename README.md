# ReMan - A Rust reimplementation of hangman :D

Want to install? Check out the binaries in the
[releases](https://gitlab.com/DarrienG/reman/-/releases) page or install with:
`cargo install reman`

Run like a normal binary and you'll get this!

Hit `CTRL-c` at any time to quit!

Otherwise, type a letter, and ReMan will enter it as a guessed letter.

If you guess a letter correctly, it will fill it in down below and if you guess
all of the letters correctly, it will display a happy message with a win screen.

Have fun :)

```

 ┌Guessed Letters────────────────────────────────────────────────────────────┐
 │                                                                           │
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘
 ┌Number Strikes Left────────────────────────────────────────────────────────┐
 │o o o o o                                                                  │
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘
 ┌ReMan──────────────────────────────────────────────────────────────────────┐
 │_ _ _ _ _ _                                                                │
 │                                                                           │
 │                                                                           │
 │                                                                           │
 │                                                                           │
 │                                                                           │
 │                                                                           │
 │                                                                           │
 │                                                                           │
 │                                                                           │
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

# Dev details

ReMan is written in pure rust.

I built it using rust `1.43.0`

But chances are it will work fine on older rusts.

With that said, your mileage may vary and I do not guarantee anything.

Hell I don't even guarantee it will work on the latest rust (although I'm
pretty certain it will).

ReMan has a number of tests that all check to if our hangman implementation
works as expected.

We pride ourselves on the solid testing and do our best to provide the most
solid user experience possible.

# Libraries

We use the cream of the crop when it comes to libraries.

The finest, the very cream of the crop from none other than crates.io

We use:

## tui

```
tui: 0.9.1
```

tui-rs is in my opinion, the finest terminal rendering library on the block.

Providing a simple and fluent API to composing interfaces in the terminal, you
can't go wrong with tui-rs.

## Unicode Segmentation

```
unicode-segmentation: 1.6.0
```

Unicode segmentation is the premiere library for segmenting Unicode characters.

It allows us to split and break apart strings on Unicode segmentation bounds.

Providing both a **simple** and **fluent** and **fast** API for splitting, you
simply won't find a finer library at the binary size of this one.

## Termion

```
termion: 1.5.5
```

Termion is truly the cream of the crop for working with terminal interfaces. It
doesn't have Windows compatibility, but, let's be real here, does anyone really
use Windows anymore?

Well they probably do, but Termion provides SUCH a SIMPLE, FLUENT, FAST,
T̲͉̦͙͈E̩̪̤͖̬̼̮̐̄ͣ͑̔ͫ͗RMͫ̌͗IN̞̻̮̝͉̫̐ͬ̇ͩ̎̽A͚̝͕̱̔ͨͧͦL͙ͅ, VI̯̖̫̭ͪ͊͋͌MTE̦̠͍̱̰̝̞̻ͦ̎ͯͣ͐͂ͪ̈́R͚͎̤͇̘͇̥̭̟͗̃̆̊̒͋ͤ͐ͬF͊̉͋̿̊ͫͥ͂͛ͧͨͮ͒A͔̞͖͙͉C̳̲̪̘̯̰͛̄ͦ̊̂͌E̍̅̿͋͌͊̾̿̓̀ that you just can't go wrong with it.

Why would you ever want to use another library?

Honestly I wish rust could be replaced by Termion altogether.

# ReMan (Really Evil hangMan) 😈

Now that everyone else is gone, here's the README for everyone who knows what's
going on or for people who keep losing and want to look at the repo again and
yell at me.

Ever been playing hangman and thought, boy it would be great if I could always
lose?

Well boy do I have the game for you.

ReMan is based off of a portion of a video by
[jan Masali](https://www.youtube.com/watch?v=le5uGqHKll8)

In the video, jan talks about the semantics of hangman and how it's the most fun
when the person guessing wins.

We know this, because the executioner can use any number of cheating tricks to
ensure they always win if they did.

Stealing one of the concepts from the video with the cheating executioner, we
provide 6 possible words the user could get, all with unique letters in them.

When the user guesses a letter, if one of the words contains the letter, we
remove that word from the possible pool of words.

The user has 5 guesses, and there are 6 words with unique letters, so by the
end, the user will always lose.

Give it to a friend or something and tell them reman stands for rust evil
hangman or something.

If you really want to be evil, bet them they won't be able to guess even a
single letter in the word. They can even get multiple attempts.

The building of this was streamed. You can watch the full process
[here](https://www.youtube.com/watch?v=112JbRH5V60&feature=youtu.be).
